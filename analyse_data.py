import matplotlib
matplotlib.use("TKAgg")
import matplotlib.pyplot as plt 

import os, csv, re
import numpy as np

RESULTS_PATH = "benchmark_impulse_ToMFirst_2Orders"
RESULTS_PATH = "benchmark_impulse_SelfFirst_2Orders"
RESULTS_PATH = "benchmark_impulse_SelfFirst_2Orders_DropFix"
RESULTS_PATH = "benchmark_impulse_ToMFirst_2Orders_DropFix"
# RESULTS_PATH = "benchmark_impulse_ToMFirst_2Orders_DropFix_InvertOther"
# RESULTS_PATH = "benchmark_impulse_SelfFirst_2Orders_DropFix_InvertOther"
RESULTS_PATH = "benchmark_impulse_ToMFirst_2Orders_DropFix_InvertOther_1BlindOrders"
# RESULTS_PATH = "benchmark_impulse_ToMFirst_2Orders_DropFix_1BlindOrders"
# RESULTS_PATH = "benchmark_impulse_ToMFirst_2Orders_DropFix_ToMActionLikelihoods"
# RESULTS_PATH = "benchmark_impulse_ToMFirst_2Orders_DropFix_400Steps_OnlyOnions"




LAYOUT_NAMES = {
    "forced_coordination" : "forcedSimple",
    "forced_coordinationTomato" : "forced",
    "asymetric": "asymmetricSimple",
    "asymetricTomato": "asymmetric",
    "assymetric": "asymmetricSimple",
    "assymetricTomato": "asymmetric",
    "coordination_ring": "ringSimple",
    "coordination_ringTomato": "ring",
    "multiplayer_schelling": "spaceySimple",
    "multiplayer_schellingTomato": "spacey",
    "simple": "crampedSimple",
    "simpleTomato": "cramped"
}

CONDITIONS_NOT_TO_SHOW = ("(0.6, 0.6)", "(0.6, 0.7)", "(0.6, 0.8)", "(0.6, 0.9)", "(0.6, 1)",
                        "(0.7, 0.7)", "(0.7, 0.8)", "(0.7, 0.9)", "(0.7, 1)",
                        "(0.8, 0.8)","(0.8, 0.9)","(0.8, 1)","(0.9, 0.9)", "(0.9, 1)",
                        "(0.4, 0.4)","(0.4, 0.5)","(0.4, 0.6)","(0.4, 0.7)","(0.4, 0.8)","(0.4, 0.9)","(0.4, 1)" ,
                        "(0.2, 0.7)", "(0.2, 0.8)", "(0.2, 0.9)",
                        "(0.1, 0.3)","(0.1, 0.4)","(0.1, 0.5)",
                        "(0, 0.7)","(0, 0.8)","(0, 0.9)"
                    )
                    
# CONDITIONS_NOT_TO_SHOW = set()

LAYOUT_ORDER = ["asymmetric", "spacey", "cramped", "ring", "forced"]

def collect_data(path):
    entries = os.listdir(path)

    # entries = [e for e in entries if "assymetricTomato" in e or "coordination_ring" in e
    #         or "forced_coordination" in e or "multiplayer_schelling" in e]

    results = {}
    best_conditions = {}
    bar_results = {}

    for entry in entries:
        matchObj = re.match("(.+)_(.+).scores", entry)
        if matchObj:
            layout = matchObj.group(1)
            cond = matchObj.group(2)
        vals = []
        with open(path + os.sep + entry, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=";")
            for i,row in enumerate(reader):
                if i == 0:
                    continue
                vals.append(int(row[1]))
        
        if not layout in results:
            results[layout] = {}
            bar_results[layout] = {}
            best_conditions[layout] = (0, None)

        results[layout][cond] = np.mean(vals)
        bar_results[layout][cond] = list(vals)

        if np.mean(vals) > best_conditions[layout][0] and cond != "(0, 0)":
            best_conditions[layout] = (np.mean(vals), cond)


    return results, bar_results, best_conditions

def plot_results(results, bar_results):

    array = []

    # layouts = sorted([l for l in results.keys() if "Tomato" in l])

    layouts = []
    for l in LAYOUT_ORDER:
        for _l in results.keys():
            if LAYOUT_NAMES[_l] == l:
                layouts.append(_l)

    # for layout in sorted(results):
    for layout in layouts:
        if "Tomato" in layout:
            tmp = []
            for cond in sorted(results[layout]):
                if cond not in CONDITIONS_NOT_TO_SHOW:
                    tmp.append(results[layout][cond])
            array.append(tmp)
    array = np.array(array)
    # first_layout = list(results.keys())[0]
    # for cond in sorted(results[first_layout]):
    #     tmp = []
    #     for layout in sorted(results):
    #         tmp.append(results[layout][cond])
    #     array.append(tmp)

    # array = np.array(array)

    row_sums = array.max(axis=1)
    array = array / row_sums[:, np.newaxis]

    conditions = list(sorted([cond for cond in results[layouts[0]].keys() if cond not in CONDITIONS_NOT_TO_SHOW]))


    fig, ax = plt.subplots()


    ax.matshow(array)
    ax.set_xticks(np.arange(len(conditions)))
    ax.set_xticklabels(conditions)
    ax.set_yticks(np.arange(len(layouts)))
    ax.set_yticklabels([LAYOUT_NAMES[layout] for layout in layouts])

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=90, va="bottom")


    # for i, layout in enumerate(layouts):
    #     for j, cond in enumerate(conditions):
    # # for i, cond in enumerate(conditions):
    # #     for j, layout in enumerate(layouts):
    #         text = ax.text(j, i, results[layout][cond],
    #                     ha="center", va="center", color="w")

    fig.tight_layout()

    plt.savefig("heatmapToMLast.pdf", bbox_inches="tight")

    # fig, ax = plt.subplots()
    # ax.bar(np.arange(len(layouts)), [np.mean(bar_results[layout]["(0, 0)"]) for layout in sorted(bar_results) if "Tomato" in layout])
    # plt.xticks(np.arange(len(layouts)), [LAYOUT_NAMES[layout] for layout in sorted(layouts)])
    # # ax.set_xticklabels(layouts)
    # plt.setp(ax.get_xticklabels(), rotation=30, va="top")
    # fig.tight_layout()

    # plt.savefig("averageRewardsSingleAgent.pdf", bbox_inches="tight")

    plt.show()


def plot_optimal_results(bar_results, bar_results_noOrder, best_conditions, best_conditions_noOrder, bar_results_single = None):


    # layouts = sorted([l for l in best_conditions.keys() if "Tomato" in l])

    layouts = []
    for l in LAYOUT_ORDER:
        for _l in bar_results.keys():
            if LAYOUT_NAMES[_l] == l:
                layouts.append(_l)

    colors = plt.cm.tab20

    fig, ax = plt.subplots()
    
    x = np.arange(len(layouts))

    cond = "(0, 0)"
    width = 0.3/2
    ax.bar(x, [np.mean(bar_results[layout][cond]) for layout in layouts], color=colors(0), 
            yerr=[np.std(bar_results[layout][cond]) for layout in layouts], width=width, label="Both know order (SP=0)")
    ax.bar(x+width, [np.mean(bar_results_noOrder[layout][cond]) for layout in layouts], color=colors(2), 
            yerr=[np.std(bar_results_noOrder[layout][cond]) for layout in layouts], width=width, label="One knows order (SP=0)")
    ax.bar(x+width*2, [np.mean(bar_results[layout][best_conditions[layout][1]]) for layout in layouts], color=colors(1), 
            yerr=[np.std(bar_results[layout][best_conditions[layout][1]]) for layout in layouts], width=width, label="Both know order (Optimal SP)")
    ax.bar(x+width*3, [np.mean(bar_results_noOrder[layout][best_conditions_noOrder[layout][1]]) for layout in layouts], color=colors(3), 
            yerr=[np.std(bar_results_noOrder[layout][best_conditions_noOrder[layout][1]]) for layout in layouts], width=width, label="One knows order (Optimal SP)")
    if results_single:
        ax.bar(x+width*4, [np.mean(bar_results_single[layout][cond]) for layout in layouts], color=colors(4), 
            yerr=[np.std(bar_results_single[layout][cond]) for layout in layouts], width=width, label="Single Agent")
    plt.xticks(np.arange(len(layouts)), [LAYOUT_NAMES[layout] for layout in layouts])
    ax.set_xticks(x+1.5*width)
    # ax.set_xticklabels(layouts)
    plt.setp(ax.get_xticklabels(), rotation=30, va="top")
    ax.legend()
    fig.tight_layout()
    

    plt.savefig("optimalAverageRewards.pdf", bbox_inches="tight")

    plt.show()


def print_scores(bar_results, best_conditions, condition=None):


    for layout in sorted(bar_results):
        if condition:
            cond = condition 
        else:
            cond = best_conditions[layout][1]
        vals = bar_results[layout][cond]
        print("Layout {}: {} ({}) - cond: {}".format(layout, np.mean(vals), np.std(vals), cond))


if __name__ == "__main__":
    #FOR PAPER
    RESULTS_PATH = "benchmark_impulse_ToMFirst_2Orders_DropFix_400Steps"
    RESULTS_PATH_NOOrder = "benchmark_impulse_ToMFirst_2Orders_DropFix_400Steps_NoOrders"

    RESULTS_PATH_ToMLast = "benchmark_impulse_ToMLast_2Orders_DropFix_400Steps"

    RESULTS_SINGLE_AGENT = "benchmark_impulse_ToMFirst_2Orders_DropFix_400Steps_SingleAgent"

    TEST = "benchmark_impulse_ToMFirst_2Orders_DropFix_400Steps_OnlyRing"
    results, bar_results, best_conditions = collect_data(RESULTS_PATH)
    # plot_results(results, bar_results)
    results_noOrder, bar_results_noOrder, best_conditions_noOrder = collect_data(RESULTS_PATH_NOOrder)

    results_single, bar_results_single, best_conditions_single = collect_data(RESULTS_SINGLE_AGENT)

    plot_optimal_results(bar_results, bar_results_noOrder, best_conditions, best_conditions_noOrder, bar_results_single)

    # print_scores(bar_results, best_conditions, condition=None)


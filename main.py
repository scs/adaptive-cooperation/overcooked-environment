import time, logging, argparse, pathlib, random

from overcooked_env.io_server import IOControl
from overcooked_env.environment import OvercookedGridworld

def parse_arguments():
    parser = argparse.ArgumentParser(description="Overcooked environment.")
    parser.add_argument("-b", "--benchmark", default=False, action="store_true")
    parser.add_argument("-l", "--local_benchmark", default=False, action="store_true")
    parser.add_argument('layout', nargs="?", type=str, default="simple.layout")                                     
    args = parser.parse_args()
    return args


def log_score(score, completed_orders, iteration, log_file):
    file_path = pathlib.Path(log_file)
    file_path.parents[0].mkdir(parents=True, exist_ok=True)
    if file_path.is_file():
        with open(log_file, "a") as f:
            f.write("{}; {}; {}\n".format(iteration, score, completed_orders))
    else:
        with open(log_file, "w") as f:
            f.write("Iteration; Score; Completed Orders\n")
            f.write("{}; {}; {}\n".format(iteration, score, completed_orders))

def evaluate_local():
    import sys, os
    sys.path.append("../cooperative-agent")
    import cooperative_agent
    from cooperative_agent import Config
    from cooperative_agent.agent import Agent

    config_path = os.path.dirname(os.path.dirname(cooperative_agent.__file__))
    config_path += os.sep + "configs"

    logging.basicConfig(level=logging.ERROR)
    layouts =   [
                    # "asymetric",
                    "asymetricTomato", 
                    # "coordination_ring", 
                    "coordination_ringTomato", 
                    # "forced_coordination", 
                    "forced_coordinationTomato", 
                    # "multiplayer_schelling",
                    "multiplayer_schellingTomato", 
                    # "simple",
                    "simpleTomato"
                ]
              
    # Different combinations of ToM_K
    # agent_configurations = [
    #                         (  0,  0), 
    #                         (  0,  1), (  0,0.5), (  0,0.3), (  0,0.1),
    #                         (  1,  1), (  1,0.5), (  1,0.3), (  1,0.1),
    #                         (0.5,0.5), (0.5,0.3), (0.5,0.1),
    #                         (0.3,0.3), (0.3,0.1), (0.1,0.1)
    #                     ]

    agent_configurations = [
                            (0,  0), (0,0.1), (0,0.2), (  0,0.3), (0,0.4), (0,0.5), (0,0.6), (0,0.7), (0,0.8), (0,0.9), (0, 1),
                                       (0.1,0.1), (0.1,0.2), (0.1,0.3), (0.1,0.4), (0.1,0.5), (0.1,0.6), (0.1,0.7), (0.1,0.8), (0.1,0.9),(0.1, 1),
                                                (0.2,0.2), (0.2,0.3), (0.2,0.4), (0.2,0.5), (0.2,0.6), (0.2,0.7), (0.2,0.8), (0.2,0.9),(0.2, 1),
                                                  (0.3,0.3), (0.3,0.4), (0.3,0.5), (0.3,0.6), (0.3,0.7), (0.3,0.8), (0.3,0.9), (0.3, 1),
                                                             (0.4,0.4), (0.4,0.5), (0.4,0.6), (0.4,0.7), (0.4,0.8), (0.4,0.9), (0.4, 1),
                                                                         (0.5,0.5), (0.5,0.6), (0.5,0.7), (0.5,0.8), (0.5,0.9), (0.5, 1),
                                                                                    (0.6,0.6), (0.6,0.7), (0.6,0.8), (0.6,0.9), (0.6, 1),
                                                                                                (0.7,0.7), (0.7,0.8), (0.7,0.9), (0.7, 1),
                                                                                                            (0.8,0.8), (0.8,0.9), (0.8,1),
                                                                                                                        (0.9,0.9), (0.9,1),
                                                                                                                                    (1, 1)
                        ]


    agent_configurations = [(0, 0)]

    MAX_TIME = 400
    repeats = 20

    env = OvercookedGridworld()
    ctrl = IOControl(env)

    start_total = time.time()
    for layout in layouts:
        start_layout = time.time()
        print("\nStarting to benchmark with layout {}".format(layout))
        for agent_conf in agent_configurations:

            agent_config1 = Config(config_path, "base_conf.json", read_all=True)
            agent_config2 = Config(config_path, "base_conf.json", read_all=True)
            agent_config1.parameters["ToM_K"] = agent_conf[0]
            # agent_config1.parameters["perform_ToM"] = agent_conf[0] != None
            agent_config2.parameters["ToM_K"] = agent_conf[1]
            # agent_config2.parameters["perform_ToM"] = agent_conf[1] != None
            # agent_config2.parameters["Perceive_Orders"] = False

            start_agent_conf = time.time()
            for i in range(repeats):
                start = time.time()
                env.players.clear()
                env.load_env("{}.layout".format(layout))
                env.required_players = 1 #2 # Hardcode here so that we do not modify the layout files
                env.time_left = MAX_TIME
                
                shuffled_confs = [agent_config1, agent_config2]
                random.shuffle(shuffled_confs)

                agent1 = Agent(shuffled_confs[0])
                # agent2 = Agent(shuffled_confs[1])

                env_id1 = env.register_player(0)["id"]
                agent1.env_id = env_id1
                # env_id2 = env.register_player(1)["id"]
                # agent2.env_id = env_id2
                env.running = True
                # Run manually so that we can do the next loop after it is done
                done = False
                last_obs = None
                while not done:
                    if env.ready_to_run():
                        obs, reward, done, info = env.step()
                        if obs != last_obs:
                            info["changed"] = True 
                        else:
                            info["changed"] = False 
                        msg = {"type": "overcookedState", "env": obs, "reward": reward, "info":info}
                        # TODO Consider using multiprocessing here
                        _id1, action1 = agent1.step(msg)
                        # _id2, action2 = agent2.step(msg)
                        env.set_action(_id1, action1)
                        # env.set_action(_id2, action2)
                        last_obs = obs

                print("Iteration {} took: {}".format(i, time.time()-start))
            
                log_score(env.score, env.completed_orders, i, "benchmark_impulse_ToMFirst_2Orders_DropFix_400Steps_SingleAgent/{}_{}.scores".format(layout, agent_conf))
            print("Agent conf {} took: {}".format(agent_conf, time.time()-start_agent_conf))
        print("Layout {} took: {}\n".format(layout, time.time()-start_layout))
    print("Finished all conditions in {}".format(time.time()-start_total))

def evaluate():
    logging.basicConfig(level=logging.ERROR)
    conditions = ["assymetricTomato"]#, "coordination_ring", "forced_coordination", "multiplayer_schellingTomato", "simple"]
    MAX_TIME = 300
    repeats = 10
    FRAMERATE = 18

    env = OvercookedGridworld()
    ctrl = IOControl(env)

    for condition in conditions:
        for i in range(repeats):
            env.load_env("{}.layout".format(condition))
            env.required_players = 2 # Hardcode here so that we do not modify the layout files
            env.time_left = MAX_TIME
            env.running = True

            # Run manually so that we can do the next loop after it is done
            done = False
            last_obs = None
            while not done:
                if env.ready_to_run():
                    obs, reward, done, info = env.step()
                    if obs != last_obs:
                        info["changed"] = True 
                    else:
                        info["changed"] = False 
                    ctrl.inform_observers(obs, reward, info)
                    last_obs = obs
                    time.sleep(1/FRAMERATE)
            
            log_score(env.score, env.completed_orders, i, "benchmark/{}.scores".format(condition))


if __name__ == "__main__":
    

    args = parse_arguments()


    

    if args.benchmark:
        evaluate()
    elif args.local_benchmark:
        evaluate_local()
    else:
        logging.basicConfig(level=logging.DEBUG)
        # logging.getLogger('io_server').setLevel(logging.INFO)
        logging.getLogger('connectionManager').setLevel(logging.ERROR)

        env = OvercookedGridworld(args.layout)
        ctrl = IOControl(env)

        framerate = 2
        env.run(framerate=framerate) # Will block indefinetely

    # 
    # last_obs = None
    # while True:
    #     obs, reward, done, info = env.step()
    #     if obs != last_obs:
    #         info["changed"] = True 
    #     else:
    #         info["changed"] = False 
    #     ctrl.inform_observers(obs, reward, info)
    #     last_obs = obs
    #     time.sleep(1/framerate)

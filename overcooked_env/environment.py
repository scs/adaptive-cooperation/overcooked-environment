import json, logging, random, time, threading
from .layouts import load_layout_dict, get_layouts

logger = logging.getLogger(__name__)

player_lock = threading.Lock()

class Direction(object):
    """
        Directions an agent can move or face to.
    """

    NORTH = (0, -1)
    SOUTH = (0, 1)
    WEST = (-1, 0)
    EAST = (1, 0)

    ACTION_TO_DIR = {
        "Up": NORTH,
        "Down": SOUTH,
        "Left": WEST,
        "Right": EAST,
    }

class Action(object):
    """
        Possible Actions that are allowed within the environment,
        together with some useful functions.
    """

    WAIT = (0,0)
    INTERACT = "Interact"

    @staticmethod
    def is_movement(action):
        return action in Direction.ACTION_TO_DIR.keys()

    @staticmethod
    def is_interaction(action):
        return action == Action.INTERACT

class Types(object):
    """
        Definition of the different types of all entities in the environment.
        Types are also grouped by shared properties.
        Also includes the symbols used to define environments from strings.

    """

    GROUND = "Ground"
    COUNTER = "Counter"
    WALL = "Wall"
    ONION_DISPENSER = "OnionDispenser"
    TOMATO_DISPENSER = "TomatoDispenser"
    PLATE_DISPENSER = "DishDispenser"
    PLATE = "Dish"
    POT = "Pot"
    SERVING = "Serving"
    SOUP_COOKING = "Soup-Cooking"
    SOUP_DISH = "Soup-Dish"
    ONION = "Onion"
    TOMATO = "Tomato"
    PLAYER = "Player"

    SYMBOLS_TO_TYPES = {
        "G": GROUND,
        "W": WALL,
        "O": ONION_DISPENSER,
        "T": TOMATO_DISPENSER,
        "D": PLATE_DISPENSER,
        "P": POT,
        "S": SERVING,
        "A": PLAYER,
        "X": COUNTER
    }

    ORDERS_TO_TYPES = {
        "Tomato": TOMATO,
        "Onion": ONION
    }


    TYPES_TO_SYMBOLS = {_type: symbol for symbol, _type in SYMBOLS_TO_TYPES.items()}

    CARRYABLE = (TOMATO, ONION, PLATE, SOUP_DISH)

    COOKABLE = (TOMATO, ONION)

    DISPENSERS = (ONION_DISPENSER, PLATE_DISPENSER, TOMATO_DISPENSER)

    @staticmethod
    def from_symbol(symbol):
        return Types.SYMBOLS_TO_TYPES[symbol]

class Recipe(object):
    """
        Preliminary definition of a recipe. Currently, we are
        only interested in the number of required ingredients in a pot 
        (as we currently only have single-ingredient recipes)
    """

    COOK_TIMES = {
        Types.ONION: 20,
        Types.TOMATO: 15
    }

    NUM_INGREDIENTS = {
        Types.ONION: 3,
        Types.TOMATO: 3
    }

    SCORES = {
        Types.ONION: 20,
        Types.TOMATO: 15
    }


class Object(object):
    """
        Base class for all "objects" in the environment, with objects
        being entities that can move or be moved within the environment.
        For carryable objects (tomato, onion or plate) this class is 
        sufficient. Player objects are more spezialized.
    """

    def __init__(self, _type, pos):
        self.type = Types.from_symbol(_type) if len(_type) == 1 else _type
        self._pos = pos
        self.is_carried = False
        self.extras = {}

    def to_json(self):
        state = {
            "type": self.type,
            "pos": self._pos,
            "carried": self.is_carried
        }
        state.update(self.extras)
        return state

    @classmethod
    def from_type(cls, _type):
        return cls(_type, None)


class Player(Object):
    """
        Spezialized "object" for Players. 
    """

    def __init__(self, player_id=0, pos=(0,0)):
        super(Player,self).__init__(Types.PLAYER, pos)
        self.player_id = player_id 
        self.controlled = True
        # Set in reset()
        # self.orientation 
        # self.in_hands
        self.reset()

    def reset(self):
        self.orientation = Direction.NORTH
        self.in_hands = None

    @property
    def pos(self):
        return self._pos

    @pos.setter
    def pos(self, pos):
        self._pos = pos 
        if self.in_hands:
            self.in_hands._pos = pos

    @property
    def front(self):
        return (self._pos[0] + self.orientation[0], self._pos[1] + self.orientation[1])

    def pickup(self, obj, tile=None):
        obj._pos = self._pos 
        obj.is_carried = True
        self.in_hands = obj
        if tile is not None:
            tile.occupied_by = None

    def drop(self, tile):
        tile.receive(self.in_hands)
        self.in_hands._pos = self.front
        self.in_hands = None 

    def get_soup(self, pot_tile):
        self.in_hands.type = Types.SOUP_DISH
        self.in_hands.extras["flavor"] = pot_tile.currently_cooking
        pot_tile.empty()
    
    def move(self, action):
        if not Action.is_movement(action):
            return self._pos
        action_dir = Direction.ACTION_TO_DIR[action]
        x,y = self._pos 
        dx, dy = action_dir
        return (x+dx, y+dy)

    def to_json(self):
        state = super(Player,self).to_json()
        # state = json.loads(_superstate)
        state.update({
            "id": self.player_id,
            "pos": self._pos,
            "orientation": self.orientation,
            "in_hands": None if self.in_hands is None else self.in_hands.to_json()
        })
        # return json.dumps(state)
        return state


class Tile(object):
    """
        Class for different types of tiles within the environment.
    """

    PASSABLES = {
        "Ground": True
    }

    def __init__(self, symbol, pos=(None,None)):
        self.pos = pos
        self.symbol = symbol
        self.type = Types.from_symbol(symbol)
        self.occupied_by = None #

    def receive(self, obj):
        self.occupied_by = obj

    @property
    def free(self):
        return self.occupied_by is None and Tile.PASSABLES.get(self.type, False)

    @property
    def potentially_free(self):
        return self.free or isinstance(self.occupied_by, Player)
        
    @property
    def passable(self):
        return Tile.PASSABLES.get(self.type, False)

    def can_drop_on(self, obj):
        return not self.occupied_by and not self.is_dispenser and not self.type == Types.SERVING

    @property
    def is_dispenser(self):
        return self.type in Types.DISPENSERS

    def produce_object(self):
        if self.is_dispenser:
            if self.type == Types.ONION_DISPENSER:
                return Object.from_type(Types.ONION)
            elif self.type == Types.TOMATO_DISPENSER:
                return Object.from_type(Types.TOMATO)
            elif self.type == Types.PLATE_DISPENSER:
                return Object.from_type(Types.PLATE)

    def to_json(self):
        state = {
            "type": self.type,
            "occupied_by": "" if self.occupied_by is None else self.occupied_by.to_json(),
            "passable": self.free
        }
        # return json.dumps(state)
        return state

    @classmethod
    def default_wall(cls):
        return cls("W")

    @classmethod
    def from_symbol(cls, symbol, pos):
        if symbol == Types.TYPES_TO_SYMBOLS[Types.POT]:
            return Pot(pos)
        else:
            return cls(symbol, pos)

class Pot(Tile):
    """
        Specialization for a pot tile, which can handle cooking.
        If we want to be able to carry pots at some point (i.e. seperate stoves and pots),
        we may need to revisist this, or at least rename this to stove and introduce the
        Pot-Object, but for now I follow the overcooked_ai approach of considering the pot 
        to be a tile.
    """

    def __init__(self, pos):
        super(Pot, self).__init__(Types.TYPES_TO_SYMBOLS[Types.POT], pos)
        self.content = []
        self.currently_cooking = None
        self.cook_time = 0

    def empty(self):
        self.cook_time = 0
        self.content = []
        self.currently_cooking = None

    def receive(self, obj):
        if self.currently_cooking != None and obj.type != self.currently_cooking:
            raise AttributeError("Cannot place {} into a pot that is cooking {}".format(obj.to_json(), self.currently_cooking))
        if len(self.content) == Recipe.NUM_INGREDIENTS.get(self.currently_cooking, -1):
            raise AttributeError("Cannot drop object into a full pot (Content: {})".format(self.content))
        self.content.append(obj)
        if self.currently_cooking is None:
            self.currently_cooking = obj.type

    def can_drop_on(self, obj):
        # For now do not allow to drop different ingredients into the pot
        if not obj.type in Types.COOKABLE:
            return False
        return self.currently_cooking is None or (len(self.content) < Recipe.NUM_INGREDIENTS[self.currently_cooking] 
                                                and obj.type == self.currently_cooking)

    @property
    def filled(self):
        return len(self.content) == 3

    @property
    def done_cooking(self):
        return self.cook_time == Recipe.COOK_TIMES.get(self.currently_cooking, None)

    def tick(self):
        if self.filled and not self.done_cooking:
            self.cook_time += 1

    def generate_soup_obj(self):
        return {
            "type": Types.SOUP_COOKING,
            "flavor": self.currently_cooking,
            "cook_time": self.cook_time,
            "n_ingredients": len(self.content),
            "ready": self.done_cooking,
            "pos": self.pos
        }

    def to_json(self):
        state = {
            "type": self.type,
            "occupied_by": "" if self.currently_cooking is None else self.generate_soup_obj(),
            "passable": self.free
        }
        # return json.dumps(state)
        return state

class OvercookedGridworld(object):
    """
        Actual environment object that accepts, interprets and handels 
        agent actions as well as environemnt interactions.
    """

    def __init__(self, env_name = None):
        self.height = None
        self.width = None
        self.env_name = env_name
        # self.grid_string = grid_string
        self.recipes = []
        self.initial_player_positions = []
        self.grid = {}
        self.players = {}
        self.pots = []
        self.objects = []
        self.orders = []
        self.time_left = -1 #Infinite
        self.concurrent_orders = 0
        self.completed_orders = []
        self.total_orders = 0
        self.player_actions = {}
        self.score = 0
        self.possible_orders = set()
        self.running = False
        # self.reset()
        if env_name:
            self.load_env(env_name)
            self.running = True
        self.stepper = self.perform_step()

    def get_layouts(self):
        return get_layouts()

    def load_env(self, env_name):
        layout_dict = load_layout_dict(env_name)
        grid = layout_dict["grid"]
        # Clean first before resetting
        grid_string = "\n".join([layout_row.strip() for layout_row in grid.split("\n")])
        self.grid_string = grid_string
        self.possible_orders = set()
        for order in layout_dict["orders"]:
            self.possible_orders.add(Types.ORDERS_TO_TYPES[order])
        self.concurrent_orders = layout_dict.get("num_concurrent_orders", 1)
        self.total_orders = layout_dict.get("total_orders", -1)
        self.time_left = layout_dict.get("total_time", -1)
        self.required_players = layout_dict.get("required_players", -1)
        self.initial_player_positions = []
        for player in self.players.values():
            player.reset()
        self.reset()

    def reset(self):
        # Generate grid from grid_string
        grid_string = self.grid_string.split("\n")
        grid = {}
        for y, row in enumerate(grid_string):
            for x, tile_string in enumerate(row):
                if tile_string != Types.TYPES_TO_SYMBOLS[Types.PLAYER]:
                    grid[(x,y)] = Tile.from_symbol(tile_string, (x,y))
                else:
                    grid[(x,y)] = Tile(Types.TYPES_TO_SYMBOLS[Types.GROUND], (x,y))
                    if not (x,y) in self.initial_player_positions:
                        self.initial_player_positions.append((x,y))
        self.height = len(grid_string)
        self.width = len(grid_string[0])
        self.grid = grid
        # Reset players at initial positions
        for player in self.players.values():
            player.pos = self.initial_player_positions[player.player_id]
            self.grid[player.pos].occupied_by = player
            player.reset()
        # Clear dynamic objects and score
        self.score = 0
        self.completed_orders = []
        self.objects = []
        self.setup_initial_orders()

    def setup_initial_orders(self):
        # TODO configure if there should be infinite or not or even a predetermined order
        self.orders = [random.choice(list(self.possible_orders)) for i in range(self.concurrent_orders)]


    def register_player(self, desired_agent_id):
        # Check if we already have such a player and this player is not currently controlled
        if desired_agent_id in self.players and not self.players[desired_agent_id].controlled:
            self.players[desired_agent_id].controlled = True
            return {"type": "playerAccepted", "id": desired_agent_id}
        else:
            # See if we can assign a new id
            new_id = len(self.players)
            if new_id < len(self.initial_player_positions):
                logger.info("Accpeted player with new id: {}".format(new_id))
                pos = self.initial_player_positions[new_id]
                if self.grid[pos].occupied_by:
                    pos = self.get_empty_ground()
                
                if pos:
                    player = Player(new_id, pos)
                    self.players[new_id] = player
                    self.grid[player.pos].occupied_by = player
                    return {"type": "playerAccepted", "id": player.player_id}
        
        logger.error("This environment does not support another player.")
        return {"type": "playerDeclined"}

    def disconnect_player(self, player_id):
        logger.info("Player {} disconnected".format(player_id))
        if player_id in self.players:
            self.players[player_id].controlled = False

    def get_empty_ground(self):
        for pos, el in self.grid.items():
            if el.type == Types.GROUND and el.free: 
                return pos
        return None

    def render(self):
        # Generate string representation from grid
        state = []
        for y in range(self.height):
            row = []
            for x in range(self.width):
                row.append(self.grid[(x,y)].to_json())
            state.append(row)

        for player in self.players.values():
            if self.grid[player.pos].occupied_by and self.grid[player.pos].occupied_by.type == Types.PLAYER:
                pass 
            else:
                logger.error("Player {} not present in grid.".format(player.to_json()))
                raise AttributeError("Render problem")
        return state

    def set_action(self, player_id, player_action):
        # TODO Make threadsafe
        with player_lock:
            self.player_actions[player_id] = player_action

    def get_info(self):
        return {"score": self.score, 
                "orders": self.orders if len(self.orders) > 0 else ""
                } 

    def step(self, actions=None):
        """
            Advances the environment's state by 1 timestep, given the current joint actions
            of all agents.

            Returns
            --------
            observations, reward, done, info
        """
        # Reset cur_reward, may be modified within this function, e.g. 
        # during interaction (or movement) handling
        self.cur_reward = 0
        if actions is None:
            actions = self._get_actions()
        # handle interactions
        self.handle_interactions(actions)
        # handle movement
        self.handle_movement(actions)
        # handle events (timings)
        self.tick_pots()
        # TODO: Countdown total time

        if self.time_left > 0:
            self.time_left -= 1

        new_state = self.render()
        reward = self.cur_reward
        done = (len(self.completed_orders) == self.total_orders or self.time_left == 0)
        info = self.get_info()
        logger.debug("Performed step with actions: {}".format(actions))
        return new_state, reward, done, info

    def tick_pots(self):
        for tile in self.grid.values():
            if tile.type == Types.POT:
                tile.tick()

    def handle_movement(self, joint_actions):
        new_positions = {}
        old_positions = {player_id: player.pos  for player_id, player in self.players.items()}
        for player_id, action in joint_actions.items():
            new_pos = self.players[player_id].move(action)
            # Check env collisions
            new_positions[player_id] = new_pos if self._valid_pos(new_pos) else old_positions[player_id]
        
        new_positions = self._handle_collisions(old_positions, new_positions)

        # print("joint actions: ", joint_actions)
        # print("new positions: ", new_positions)
        tiles_to_clear = set()
        # Move players and update tiles
        for player_id in joint_actions:
            player = self.players[player_id]
            # old_tile = self.grid[player.pos]
            tiles_to_clear.add(player.pos)
            # old_tile.occupied_by = None 
            player.pos = new_positions[player_id]
            player_action = joint_actions[player_id]
            if Action.is_movement(player_action):
                player.orientation = Direction.ACTION_TO_DIR[player_action]
            new_tile = self.grid[player.pos]
            new_tile.occupied_by = player

        for tile in tiles_to_clear-set(new_positions.values()):
            self.grid[tile].occupied_by = None
        
        # for player in self.players.values():
        #     print("new player pos: ", player.pos)
        #     print("new player tile: ", self.grid[player.pos].to_json())

    def _valid_pos(self, pos):
        return self.grid.get(pos, Tile.default_wall()).potentially_free 


    def _handle_collisions(self, old_positions, new_positions):
        """If agents collide, they stay at their old locations"""
        # for idx0, idx1 in itertools.combinations(range(len(old_positions)), 2):
        #     pi_old, pj_old = old_positions[idx0], old_positions[idx1]
        #     pi_new, pj_new = new_positions[idx0], new_positions[idx1]
        colliding_players = set()
        for player_id_1 in new_positions:
            for player_id_2 in new_positions:
                if player_id_1 != player_id_2 and new_positions[player_id_1] == new_positions[player_id_2]:
                    # Players collide: store ids
                    colliding_players.add(player_id_1)
                    colliding_players.add(player_id_2)

        if colliding_players:
            for player in colliding_players:
                new_positions[player] = old_positions[player]
            new_positions = self._handle_collisions(old_positions, new_positions)



        # if self.is_transition_collision(old_positions, new_positions):
        #     return old_positions
        return new_positions
        

    def _get_actions(self):
        # TODO: Make threadsafe!
        with player_lock:
            actions= {player_id : self.player_actions.get(player_id, "Wait") 
                            for player_id in self.players}

            for player_id in self.players:
                if not player_id in self.player_actions:
                    logger.error("No action for player: {}".format(player_id))
            self.player_actions.clear()
        return actions

    def handle_interactions(self, joint_actions):
        for player_id, action in joint_actions.items():
            if action == Action.INTERACT:
                self.handle_interaction(player_id)

    def handle_interaction(self, player_id):
        player = self.players[player_id]
        tile_in_front = self.grid[player.front]
        if player.in_hands:
            # Check if we could place our object down in front of us
            if tile_in_front.can_drop_on(player.in_hands):
                player.drop(tile_in_front)
                
            # Check if we have a plate in hand and a pot in front of us
            elif player.in_hands.type == Types.PLATE and tile_in_front.type == Types.POT:
                if tile_in_front.done_cooking:
                    player.get_soup(tile_in_front)
            elif player.in_hands.type == Types.SOUP_DISH and tile_in_front.type == Types.SERVING:
                soup = player.in_hands
                # Deliver soup
                player.drop(tile_in_front)
                # Check if soup was ordered
                logger.debug("orders: {}, soup flavor: {}".format(self.orders, soup.extras["flavor"]))
                if soup.extras["flavor"] in self.orders:
                    logger.debug("Increase score")
                    self.cur_reward = Recipe.SCORES[soup.extras["flavor"]]
                    self.score += self.cur_reward
                    self.orders.remove(soup.extras["flavor"])
                    self.completed_orders.append(soup.extras["flavor"])
                    self.setup_next_order()
                # Clean up serving tile
                tile_in_front.occupied_by = None
        else:
            # Check if there is something in front of us, we could pick up
            if tile_in_front.is_dispenser:
                new_obj = tile_in_front.produce_object()
                self.objects.append(new_obj)
                player.pickup(new_obj)
            elif tile_in_front.occupied_by:
                obj = tile_in_front.occupied_by
                if obj.type in Types.CARRYABLE:
                    player.pickup(obj, tile_in_front)

    def setup_next_order(self):
        # TODO configure if there should be infinite or not or even a predetermined order
        self.orders.append(random.choice(list(self.possible_orders)))

    def inform_observers(self, observation, reward, info):
        """
            Since we are currently only considering IO observer, this is handled by the
            IOControl.
        """
        raise NotImplementedError("This should be set by the observer for now.")

    def toggle_pause(self):
        print("toogle pause")
        self.running = not self.running
        logger.debug("Toggled running to: {}".format(self.running))


    def perform_step(self):
        last_obs = None
        while True:
            obs, reward, done, info = self.step()
            if obs != last_obs:
                info["changed"] = True 
            else:
                info["changed"] = False 
            self.inform_observers(obs, reward, info)
            last_obs = obs
            yield

    def manual_step(self):
        next(self.stepper)

    def ready_to_run(self):
        return len(self.players) >= self.required_players

    def run(self, framerate=10):
        """
            Runs the environment with the given framerate.
            Each tick, the environment will perform a "step",
            notifying any registered observers of the results, then waits
            for the next step.
        """
        while True:
            if self.running:
                if self.ready_to_run():
                    next(self.stepper)
                else:
                    logging.info("Still waiting for more players. {} ({} required)".format(len(self.players), self.required_players))
                # obs, reward, done, info = self.step()
                # if obs != last_obs:
                #     info["changed"] = True 
                # else:
                #     info["changed"] = False 
                # self.inform_observers(obs, reward, info)
                # last_obs = obs
            time.sleep(1.0/framerate)


# -*- coding: utf-8 -*-
"""
Created on Sat Jun 24 00:00:38 2017
Abstracts multiple connection possibilities away.
@author: Jan
"""

try:
    import zmq
    zmq_available = True
except (ImportError):
    zmq_available = False

import threading, logging, time, json

from connectionManager import ConnectionManager, get_params

MAX_CONNECTIONS = 5

logger = logging.getLogger(__name__)

mutex = threading.Lock()

class IOControl(object):
    
    def __init__(self, env, config=None):
        if not config:
            config = dict()
        self.env = env
        # Set the inform_observer function
        self.env.inform_observers = self.inform_observers
        self.con_manager = ConnectionManager()
        # self.con_manager.setup_zmq_server(config.get("ctrl_port", "6666"), self.handle_messages)
        server_params = get_params("zmq", "incoming")
        server_params.update({
            "connection_identifier": "overcooked_env",
            "port": config.get("ctrl_port", "6666"),
        })
        self.con_manager.open_connection("zmq", server_params, self.handle_messages)
        self.state_observers = []
        self.agent_connections = {}
        self.client_infos = {}

    def handle_messages(self, msg):
        if msg["type"] != "hello":
            logger.info("received message {} to handle".format(msg))
        if "payload" in msg:
            msg = msg["payload"]
        if msg["type"] == "setup_connection":
            if msg["connection_type"] == "state_observer":
                self.state_observers.append(msg["identifier"])
                client_params = get_params("zmq", "outgoing")
                client_params.update({
                    "connection_identifier": msg["identifier"],
                    "port": msg["connection_params"]["port"],
                    "client_ident": msg["identifier"]
                })
                self.con_manager.open_connection("zmq", client_params, self.handle_con_messages)
                info = self.env.get_info()
                info["changed"] = True
                self.con_manager.notify(msg["identifier"], {"type": "overcookedState", "env": self.env.render(), "reward": 0, "info": info})

            if msg["connection_type"] == "acting_observer":
                logger.info("registering new player")
                res = self.env.register_player(msg["desired_agent_id"])
                client_params = get_params("zmq", "outgoing")
                client_params.update({
                    "connection_identifier": msg["identifier"],
                    "port": msg["connection_params"]["port"],
                    "client_ident": msg["identifier"]
                })
                self.client_infos[msg["identifier"]] = client_params
                self.con_manager.open_connection("zmq", client_params, self.handle_con_messages)
                    
                if res["type"] == "playerAccepted":
                    self.state_observers.append(msg["identifier"])
                    self.agent_connections[msg["identifier"]] = client_params

                    self.con_manager.notify(msg["identifier"], {"type": "agentAccepted", "agent_id": res["id"]})

                    logger.info("Sending state after setting up connection")
                    info = self.env.get_info()
                    info["changed"] = True
                    self.con_manager.notify(msg["identifier"], {"type": "overcookedState", "env": self.env.render(), "reward": 0, "info": info})
                else:
                    self.con_manager.notify(msg["identifier"], {"type": "agentDenied"})

        if msg["type"] == "request_layouts":
            self.con_manager.notify(msg["identifier"], {"type": "layouts", "layouts": self.env.get_layouts()})

        if msg["type"] == "reset_layout":
            self.env.reset()
            info = self.env.get_info()
            info["changed"] = True
            info["new_env"] = True
            self.inform_observers(self.env.render(), 0, info)

        if msg["type"] == "request_agent_info":
            self.con_manager.notify(msg["identifier"], {"type": "agent_info", "agent_infos": self.client_infos})

        if msg["type"] == "toggle_pause":
            self.env.toggle_pause()

        if msg["type"] == "step_environment":
            self.env.manual_step()


        if msg["type"] == "envSelection":
            self.env.load_env(msg["layout"])
            info = self.env.get_info()
            info["changed"] = True
            info["new_env"] = True
            self.inform_observers(self.env.render(), 0, info)

    def inform_observers(self, observations, reward, info):
        with mutex:
            for con_ident in self.state_observers:
                self.con_manager.notify(con_ident, {"type": "overcookedState", "env": observations, "reward": reward, "info":info})
    #         self.con_manager.notify(con_ident, {"overcooked":observations, "reward": reward, "info":info})

    def handle_con_messages(self, msg):
        # TODO consider passing in the identifier (e.g. passing a partially completed function to the ZMQConnection)
        # In case we want/need to handle messages differently depending on where they came from (not only depending on msg type)
        logger.info("Received message: {}".format(msg))
        if msg["type"] == "userAction":
            self.env.set_action(player_id=msg["id"], player_action=msg["action"])
        if msg["type"] == "agentAction":
            self.env.set_action(player_id=msg["id"], player_action=msg["action"])
        if msg["type"] == "registerPlayer":
            reply = self.env.register_player()
            self.con_manager.notify(msg["identifier"], reply)
        if msg["type"] == "agentDisconnect":
            logger.info("Agent {} disconnected.".format(msg["id"]))
            logger.debug("state observers before: {}".format(self.state_observers))
            self.env.disconnect_player(msg["id"])
            with mutex:
                self.state_observers.remove(msg["identifier"])
            self.con_manager.close_connection(msg["identifier"])
            del self.client_infos[msg["identifier"]]
        
    def __del__(self):
        logger.info("deconstructing connectionManager")
        self.con_manager.clear()
        
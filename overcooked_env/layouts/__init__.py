import os 

BASE_PATH = os.path.dirname(os.path.abspath(__file__))

def load_layout_dict(name):
    path = BASE_PATH + os.path.sep + name 
    with open(path, "r") as f:
        return eval(f.read())

def get_layouts():
    files = os.listdir(BASE_PATH)
    return sorted([f for f in files if ".layout" in f])